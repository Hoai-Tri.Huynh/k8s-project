
# Projet Helm Chart pour K8s

Ce dépôt contient le Helm chart pour le déploiement d'une application Kubernetes multi-services. Il inclut des configurations pour des services tels que le frontend, l'email, le paiement, la publicité, et plusieurs autres services.

## Structure du Projet

Ce Helm chart comprend plusieurs manifestes Kubernetes formatés avec Helm. Voici un aperçu détaillé des fichiers inclus et leur fonction :

### `deployment.yaml`

Ce fichier YAML définit les déploiements Kubernetes pour chaque service spécifié dans `values.yaml`. Il utilise une logique conditionnelle et des boucles pour dynamiquement créer des configurations de déploiement basées sur les données fournies dans `values.yaml`. Chaque déploiement configurera les aspects suivants pour chaque service :

- **Réplicas** : Le nombre de pods à maintenir.
- **Labels** : Identifiants pour les pods pour aider à leur gestion.
- **Conteneurs** : Configuration des conteneurs, y compris l'image à utiliser, les politiques de redémarrage, les ports, et d'autres paramètres essentiels.
- **Probes de vivacité et de disponibilité** : Scripts qui vérifient la santé des conteneurs à intervalles réguliers.

### `service.yaml`

Ce fichier configure les services Kubernetes, qui permettent la communication réseau avec les pods générés par les déploiements décrits dans `deployment.yaml`. Chaque service définit :
  
- **Type de service** : Le type de service, comme ClusterIP, qui détermine comment le service est exposé.
- **Ports** : Les ports sur lesquels le service est accessible, correspondant aux ports exposés par les conteneurs.

### `ingress.yaml`

Gère l'accès externe aux services à travers un objet Ingress, qui agit comme un contrôleur de trafic. Les annotations spécifiques et les hôtes sont configurés pour diriger le trafic entrant vers les services appropriés. Il inclut :

- **Class Name** : La classe d'Ingress utilisée, souvent liée à un contrôleur d'Ingress spécifique.
- **Règles** : Définissent comment le trafic entrant est routé vers les services.
- **TLS** : Configurations pour le cryptage du trafic, pointant vers les secrets pour les certificats SSL/TLS.

### `values.yaml`

Contient toutes les valeurs configurables qui contrôlent le comportement du Helm chart. Cela inclut des images de conteneurs, des tags, le nombre de répliques, des configurations réseau, et d'autres paramètres spécifiques à chaque service. Ce fichier permet une personnalisation facile sans avoir à modifier les templates eux-mêmes.

### `Chart.yaml`

Fournit des métadonnées sur le Helm chart, y compris la version du chart, la description, et le type de chart (application). Ce fichier est crucial pour la gestion de versions et les informations générales sur le chart.

## Dockerfile Setups

Chaque service dans le projet utilise un Dockerfile spécifique pour définir comment l'image du conteneur doit être construite. Voici une description de chaque Dockerfile :

### AdService

- **Base Image**: `eclipse-temurin:21`
- **Purpose**: Construit AdService en utilisant un processus de build multi-étapes pour garder l'image finale légère.
- **Exposes Port**: 9555
- **Command**: Exécute le binaire `AdService`.

### CurrencyService

- **Base Image**: `node:14`
- **Purpose**: Configure l'application Node.js CurrencyService.
- **Exposes Port**: 5000
- **Command**: Exécute `node server.js`.

### EmailService

- **Base Image**: `python:3.8-slim`
- **Purpose**: Prépare l'environnement Python et les dépendances pour EmailService.
- **Exposes Port**: 5000
- **Command**: Exécute `python email_server.py`.

### Frontend

- **Base Image**: `golang:1.22.3-alpine`
- **Purpose**: Compile le service frontend à partir de la source en utilisant Go.
- **Exposes Port**: 8080
- **Command**: Exécute le serveur Go compilé.

## Comment Déployer

1. **Préparez Votre Cluster Kubernetes** : Assurez-vous d'avoir un cluster Kubernetes en fonctionnement et que `kubectl` est configur

é pour interagir avec votre cluster.

2. **Installez Helm** : Suivez le guide d'installation de Helm pour configurer Helm sur votre machine.

3. **Déployez le Chart** : Naviguez jusqu'au répertoire contenant le Helm chart et exécutez :
   ```bash
   helm install mon-app-k8s .
   ```
   Cette commande déploie l'ensemble de l'application sur votre cluster Kubernetes.

4. **Vérifiez le Déploiement** : Vérifiez l'état des services déployés :
   ```bash
   kubectl get all