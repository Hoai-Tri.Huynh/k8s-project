{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "k8s-project.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{/*
Common labels
*/}}
{{- define "k8s-project.labels" -}}
helm.sh/chart: {{ include "k8s-project.chart" . }}
app.kubernetes.io/name: {{ include "k8s-project.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/version: {{ .Chart.AppVersion }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "k8s-project.selectorLabels" -}}
app.kubernetes.io/name: {{ include "k8s-project.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Return the name of the chart
*/}}
{{- define "k8s-project.name" -}}
{{ .Chart.Name }}
{{- end }}

{{/*
Return the version of the chart
*/}}
{{- define "k8s-project.chart" -}}
{{ .Chart.Name }}-{{ .Chart.Version }}
{{- end }}

{{/*
Return the service account name
*/}}
{{- define "k8s-project.serviceAccountName" -}}
{{ if .Values.serviceAccount.create }}
{{ default (include "k8s-project.fullname" .) .Values.serviceAccount.name }}
{{ else }}
{{ .Values.serviceAccount.name }}
{{ end }}
{{- end }}
